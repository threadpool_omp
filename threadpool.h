/*
 * Copyright 2016-2018 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP_THREADPOOL_H
#define TP_THREADPOOL_H

/**
 * The thread pool object, allocated by tp_init() and destroyed by tp_free().
 */
typedef struct TPContext TPContext;

/**
 * The callback passed to tp_execute().
 *
 * @param arg the user pointer supplied to tp_execute
 * @param job_idx the job index, between 0 and nb_jobs
 * @param thread_idx the index of the thread executing this job, between 0 and
 *                   nb_threads
 *
 * @return the callback should return 0 on success and a negative number on
 *         failure
 */
typedef int (*TPExecuteCallback)(void *arg, unsigned int job_idx, unsigned int thread_idx);

/**
 * Create the thread pool object.
 *
 * @param ctx the newly created thread pool will be written here
 * @param nb_threads the number of threads to execute in parallel
 * @return 0 on success, a negative error code on failure
 */
int  tp_init(TPContext **ctx, unsigned int nb_threads);
/**
 * Free the thread pool object and write NULL into the supplied pointer.
 */
void tp_free(TPContext **ctx);

/**
 * Execute a function multiple times in parallel.
 *
 * @param ctx the thread pool object
 * @param nb_jobs number of times to call func
 * @param func the callback to call
 * @param func_arg arbitrary user data to pass to func
 *
 * @return 0 if all the calls to func returned successfully, a negative error
 * code otherwise
 */
int tp_execute(TPContext *ctx, unsigned int nb_jobs,
               TPExecuteCallback func, void *func_arg);

/**
 * Get the number of threads in this thread pool.
 */
unsigned int tp_get_nb_threads(TPContext *ctx);

#endif /* TP_THREADPOOL_H */
