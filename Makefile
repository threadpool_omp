TARGET = libthreadpool.so

CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -fopenmp -O3 -g -I.
LDFLAGS = -Wl,--version-script=libthreadpool.v -shared -fopenmp
CC = cc

OBJS =                      \
    threadpool.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS) ${LDFLAGS}

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

clean:
	-rm -f *.o *.d $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: clean
