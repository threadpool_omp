/*
 * Copyright 2018 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <stdio.h>

#include "threadpool.h"

struct TPContext {
    unsigned int nb_threads;
};

void tp_free(TPContext **pctx)
{
    TPContext *ctx = *pctx;

    if (!ctx)
        return;

    free(ctx);
    *pctx = NULL;
}

int tp_init(TPContext **pctx, unsigned int nb_threads)
{
    TPContext *ctx = NULL;
    int ret;

    if (!nb_threads) {
        const char *env_threads = getenv("OMP_NUM_THREADS");
        if (env_threads) {
            nb_threads = strtol(env_threads, NULL, 0);
        }
#ifdef _SC_NPROCESSORS_ONLN
        else {
            long val = sysconf(_SC_NPROCESSORS_ONLN);
            if (val > 0)
                nb_threads = val;
        }
#endif

        if (!nb_threads) {
            ret = -EINVAL;
            goto fail;
        }
    }

    ctx = calloc(1, sizeof(*ctx));
    if (!ctx) {
        ret = -ENOMEM;
        goto fail;
    }

    omp_set_num_threads(nb_threads);

    ctx->nb_threads = nb_threads;

    *pctx = ctx;
    return 0;

fail:
    tp_free(&ctx);
    *pctx = NULL;
    return ret;
}

int tp_execute(TPContext *ctx, unsigned int nb_jobs,
               TPExecuteCallback func, void *func_arg)
{
#pragma omp parallel for
    for (unsigned int i = 0; i < nb_jobs; i++)
        func(func_arg, i, omp_get_thread_num());

    return 0;
}

unsigned int tp_get_nb_threads(TPContext *ctx)
{
    return ctx->nb_threads;
}
